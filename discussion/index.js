//console.log("Hello")

//OBJECTS
/*
	- is a data type that is used to represent real world objects
	- it is a collection of related data and/or functionalities
	0 info stored in objects are represented in "key:value" pair
	- different data types may be stores in a object
*/

// Creating Objects
/*
	There are 2 ways to create objects:
	1. Object initializers/literal notation
	2. Constructor
*/


// Creating objects through object initializers/literal notation
/*
	- this creates/declares an object and also initializes/assigns its values uopon creation.
	- it already has its keys and values such as name, color, weight, etc.
	- syntax:
		let/const objectName = {
			keyA: valueA;
			keyB: valueB;
		};
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);


// Creating objects using constructor functions
/*
	- creates a reusable function to create several objects that have the same data structure
	- useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it.
	- syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA,
			this.keyB = keyB
		} 
*/

function laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

function student(firstName, lastName, yearOld, city){
	this.name = firstName + " " + lastName;
	this.age = yearOld;
	this.residence = city;
}
/*
	- the "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function.
*/

// This is an instance of the laptop object
console.log("This is a unique instance of the Laptop Object using object constructor");
let laptopMica = new laptop('Lenovo', 2008);
console.log(laptopMica);

let laptopEric = new laptop('HP', 1990);
console.log(laptopEric);

console.log("This is a unique instance of the student Object using object constructor");
let elaine = new student('Elaine', 'SJ', '12', 'Mars');
console.log(elaine);

let jake = new student('Jake', 'Lexter', 10, "Sun");
console.log(jake);
/*
	- the new keyword creates an instance of an object
*/

console.log("This is a unique instance of the student Object without the new keyword");
let oldLaptop = laptop("Portal", 1980);
console.log(oldLaptop);



// Accessing Object Properties

/*
	There are two ways to access object properties
	1. Using the dot notation
*/
console.log(laptopMica.name);
console.log(elaine.name);

// 2. Using the "square bracket notation"
console.log("Result from square bracket notation: " + laptopMica['name']);
console.log("Result from square bracket notation: " + elaine['name']);



// Initializing / Adding / Delete / Reassigning Object Properties
/*
	- like any other variable in Javascript, object may have their own properties initialized after the object was created/declared
	- this is useful for times when an object's properties are undetermined at the time of beginning.
*/

// Empty Object
console.log("Empty 'car' object");
let car = {};
console.log(car);
console.log(" ");

// Initializing/Adding object properties using dot notation
car.name = "Honda Civic";
console.log(car);

car.driver = "Mr. Bean";
console.log(car);

// Initializing/Adding object properties using bracket notation
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log("Result from adding properties using square bracket notation");
console.log(car);
console.log(" ");

// Deleting object properties
delete car ['manufacture date'];
delete car.name;
console.log("Result from deleting properties");
console.log(car);
console.log(" ");

// Reasign object properties
car.name = "Dodge Charger R/T";
console.log("Result from reassigning properties");
console.log(car);
console.log(" ");



// Object Methods
/*
	- a method is a function, and attached to an object as a property
	- a method is useful for creating object specific properties
	- similar to functions of real world objects, methods are defined based on what an object is capable of doing.
*/

let person = {
	name: "John",
	talk: function (){
		console.log("Hello, my name is " + this.name);
	}
}

console.log(person);
console.log("Result from object methods");
person.talk();

//
person.walk = function(){
	console.log(this.name + " walked 25 steps forward");
}
person.walk();

//methods are useful for creating reusable functions that perform tasks related to objects

let friend = {
	firstName: 'Arjay',
	lastName: 'Dala',
	address: {
		city: 'cavite',
		country: 'USA'
	},
	emails: ['arjay@friendster.com', 'arjay@myspace.com'],
	introduce: function(classmate){
		console.log("Hello, my name is " + this.firstName + " " + this.lastName + ". Nice to meet you " + classmate)
	}
}

friend.introduce("Eric");
friend.introduce("Hatdog");



// Object Literal for pokemon
let myPokemon = {
	name: 'Pickachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon){
		console.log(this.name + " tackled " + targetPokemon);
		console.log(targetPokemon + "'s health is reduced to " + targetPokemon + "'s health")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}
console.log(myPokemon);
myPokemon.tackle('Charmander');
console.log(" ");



// create pokemon characters using constructor function
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log(target.name + "'s health now is now reduced to " + target.health)
	};
	this.faint = function(){
		console.log(this.name + 'fainted')
	}
}

let pikachu = new Pokemon('Pikachu', 16);
let ratata = new Pokemon('Ratata', 8);

pikachu.tackle(ratata);

//product
function shopeeProduct(name, initialPrice, discount){
	this.name = name;
	this.initialPrice = initialPrice;
	this.discountPercent = discount;
	this.price = initialPrice + 500;
	this.discount = function discount(value){
		let discountedPrice = this.price - value;
			console.log("The total discounted price is " + discountedPrice)
	};
	this.stock = function(quantity){
		if (quantity <= 5){
			alert("Babagsak ang negosyo");
		} else {
			alert("Goods goods pa naman");
		}
	}
}

let iphoneX = new shopeeProduct('IphoneX', 3000, 0);
iphoneX.discount(100);
iphoneX.stock(2);